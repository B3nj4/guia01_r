elementos = c("lapiz", "cuaderno", "libro", "Goma")
cantidad = c(3, 7, 15, 1)

tabla <- c(elementos, cantidad)
matriz <- matrix(tabla, ncol=2)
matriz

orden <- matriz[order(as.numeric(matriz[,2]), decreasing = F)]
orden

mayor <- factor(orden,
                ordered = T,
                levels = orden)
mayor
